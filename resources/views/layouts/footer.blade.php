<div class="box clearfix">
    <h3 class="bh_b">GIA SƯ TIÊU BIỂU</h3>
    <div class="giasutieubieu">
        <div class="jcarousel" style="visibility: visible; overflow: hidden; position: relative; height: 360px;">
            <ul style="margin: 0px; padding: 0px; position: relative; list-style-type: none;">
                @php
                    $giasu = \App\GiaSu::select('id', 'holot', 'ten', 'anhthe')->where('tieubieu', 1)->orderBy('updated_at', 'desc')->take(10)->get();
                @endphp
                @if($giasu->count() > 0)
                    @foreach($giasu as $row)
                        @if($row->anhthe != null)
                            <li style='width: 142px; height: 180px;'>
                                <a href="{{route('danhsachgiasu')}}"><img src="/thumb.php?src={{asset('storage/anhthe/'.$row->anhthe . '?v=4.1')}}&w=142&h=176" title="{{$row->holot}} {{$row->ten}}"/></a>
                            </li>
                        @else
                            <li style='width: 142px; height: 180px;'>
                                <a href="{{route('danhsachgiasu')}}"><img src="/thumb.php?src={{asset('storage/anhthe/no_image.jpg?v=4.1')}}&w=142&h=176" title="{{$row->holot}} {{$row->ten}}"/></a>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="more_g">
            <a href="{{route('danhsachgiasu')}}">Xem thêm</a>
        </div>
    </div>
</div>

<div class="box clearfix">
    <h3 class="bh_b">TIN TỨC MỚI</h3>
    <div class="jcarousel_tintuc">
        @php
            $tintuc = \App\BaiViet::select('tieude', 'slug', 'anhbia')->where('danhmuc', 1, '&and')->where('trangthai', 1)->orderBy('updated_at', 'desc')->take(10)->get();
        @endphp
        <ul class="l_tintuc" style="margin: 0px; padding: 0px; position: relative; list-style-type: none;">
            @if($tintuc->count() > 0)
                @foreach($tintuc as $row)
                    <li>
                        <a href="{{route('chitiettintuc', $row->slug)}}">
                            @if(!empty($row->anhbia))
                                <img src="/thumb.php?src={{asset('storage/'.$row->anhbia . '?v=4.1')}}&w=140&h=90">
                            @else
                                <img src="/thumb.php?src={{asset('storage/gia-su-thanh-danh-day-kem-tai-nha.jpg?v=4.1')}}&w=140&h=90">
                            @endif
                            <br>{{$row->tieude}}
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
    <div class="more_g">
        <a href="{{route('tintuc')}}">Xem thêm</a>
    </div>
</div>

<div class="box clearfix">
    <h3 class="bh_b">FANPAGE</h3>
    <iframe
        src="https://www.facebook.com/plugins/page.php?href={{config('facebook.url')}}&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&width=150"
        width="150" style="border:none;overflow:hidden" scrolling="true" frameborder="0"
        allowTransparency="true" allow="encrypted-media"></iframe>
</div>

<div class="box clearfix">
    <h3 class="bh_b">BẢN ĐỒ</h3>
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d979.5208831017687!2d106.63050362304949!3d10.881251128977821!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529ffde2cb01f%3A0x91c5a4be7f170c09!2zVHLhuqduIFRo4buLIEjDqCwgSGnhu4dwIFRow6BuaCwgUXXhuq1uIDEyLCBI4buTIENow60gTWluaCwgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1501506002415"
        width="100%" height="230" style="border:none;overflow:hidden" allowfullscreen></iframe>
</div>

<div class="box clearfix">
    <h3 class="bh_b">BỘ ĐẾM TRUY CẬP</h3>
    <img src="https://counter5.wheredoyoucomefrom.ovh/private/freecounterstat.php?c=j1pqq6kry2sxddfh4qycukypqp8azgdd"
         alt="{{config('app.name')}}" style="margin: 5px;">
</div>
</div>
</div>

<div class="footer" style="border: 1px solid #04776f00;">
    <div class="f_inner clearfix">
        <div class="f_inner_l">
            <ul class="f_list" style="border-right: none;width: 100%;">
                <li>
                    <span class="vp1" style="line-height: 1.5;">{{config('app.diachi11')}}</span><br>
                    <span style="line-height: 1.5;">{{config('app.diachi1')}}</span>
                </li>
            </ul>
        </div>

        <div class="f_inner_r">
            <ul class="f_list" style="border-right: none; width: 100%;">
                <li>
                    <span class="vp1" style="line-height: 1.5;">{{config('app.diachi22')}}</span><br>
                    <span style="line-height: 1.5;">{{config('app.diachi2')}}</span>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>
<a href="javascript:void(0);" id="scroll" title="Lên đầu trang" style="display: none; margin-bottom: 80px; margin-right: 25px;"><span></span></a>

<script src="{{asset('js/jquery-2.2.4.min.js?v=4.1')}}"></script>
<script src="{{asset('js/jcarousellite.min.js?v=4.1')}}"></script>
<script src="{{asset('js/jquery-3.4.1.min.js?v=4.1')}}"></script>
<script src="{{asset('js/jquery-ui.min.js?v=4.1')}}"></script>
<script src="{{asset('js/jquery.validate.min.js?v=4.1')}}"></script>
<script src="{{asset('js/swipe.min.js?v=4.1')}}"></script>
<script src="{{asset('js/sweetalert2.min.js?v=4.1')}}"></script>
<script src="{{asset('js/login.js?v=4.1')}}"></script>
<script src="{{asset('js/lazyload.js?v=4.1')}}"></script>
@section('footer')
@show
    <script src="{{asset('js/cookie.js?v=4.1')}}"></script>
    <script src="{{asset('js/home.js?v=4.1')}}"></script>
    @if(Session::has('status'))
        <script>
            Swal.fire({
                type: "{!!Session::get('status')!!}",
                title: "",
                text: "{!!Session::get('message')!!}",
                customClass: {
                    content: "content-{!!Session::get('status')!!}",
                    confirmButton: "btn-{!!Session::get('status')!!}",
                }
            });
        </script>
    @endif

    <script>
        $(document).ready(function () {
            $('img').each(function () {
                var src = $(this).attr("src");
                $(this).addClass("lazyload");
                $(this).attr("data-src", $(this).attr("src"));
                $(this).attr("alt", "<?php echo config('app.title');?>");
            });
            lazyload();
            $('#app_sdt1').each(function () {
                $(this).html("<?php echo config('app.sdt1');?>");
            });
            $('#app_sdt2').each(function () {
                $(this).html("<?php echo config('app.sdt2');?>");
            });
            $('#app_url').each(function () {
                $(this).attr("href", "<?php echo config('app.url');?>");
                $(this).html("<?php echo config('app.url');?>");
            });
            $('#app_email').each(function () {
                $(this).attr("href", "mailto:<?php echo config('app.email');?>");
                $(this).html("<?php echo config('app.email');?>");
            });
            $('#app_diachi11').each(function () {
                $('#app_diachi11').html("<?php echo config('app.diachi11');?>");
            });
            $('#app_diachi1').each(function () {
                $('#app_diachi1').html("<?php echo config('app.diachi1');?>");
            });
            $('#app_diachi22').each(function () {
                $('#app_diachi22').html("<?php echo config('app.diachi22');?>");
            });
            $('#app_diachi2').each(function () {
                $('#app_diachi2').html("<?php echo config('app.diachi2');?>");
            });
        });
    </script>
    <div id='fb-fanbox'>
        <div id='fb-boxclose'></div>
        <div id='fb-boxview'>
            <div id='fb-closebox'></div>
            <iframe allowtransparency="true" frameborder="0" scrolling="no" src="https://www.facebook.com/plugins/likebox.php?href={{config('facebook.url')}}&width=500&height=300&colorscheme=light&show_faces=true&show_border=false&stream=false&header=false" style="border:none;overflow:hidden;width:500px;height:300px;"></iframe>
            </div>
    </div>

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <!-- Your customer chat code -->
    <div class="fb-customerchat" attribution=setup_tool page_id="{{config('facebook.id')}}" logged_in_greeting="{{config('facebook.greeting')}}" logged_out_greeting="{{config('facebook.greeting')}}" theme_color="{{config('facebook.color') ? config('facebook.color') : '#0084ff'}}"></div>
</div>
</div>
</body>
</html>
