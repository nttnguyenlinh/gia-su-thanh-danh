<!DOCTYPE html>
<html lang="vi">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="content-language" content="vi">
    <meta http-equiv="Cache-control" content="private, max-age=604800">
    <title>@yield('title'){{config('app.title')}}</title>

    <!-- ROBOTS -->
    <meta name="googlebot" content="noarchive" />
    <meta name="robots" content="noarchive" />

    <!-- GENERAL GOOGLE SEARCH META -->
    <meta name="abstract" content="{{config('app.title')}}">
    <meta rel="canonical" href="@yield('canonical')">
    <meta name="RATING" content="GENERAL">
    <meta name="description" content="{{config('app.description')}}">
    <meta name="keywords" content="{{config('app.keywords')}}">
    <meta name="geo.placename" content="Ho Chi Minh, Viet Nam">
    <meta name="geo.posion" content="Ho Chi Minh, Viet Nam">
    <meta property="og:title" content="@yield('title'){{config('app.title')}}">
    <meta property="og:url" content="@yield('url')">
    <meta property="og:site_name" content="{{config('app.name')}}">
    <meta property="og:description" content="{{config('app.description')}}">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="vi_VN">
    <meta property="og:image" content="{{asset(config('app.banner').'?v=4.1')}}">
    <meta property="og:image:secure_url" content="{{asset(config('app.banner').'?v=4.1')}}">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:rich_attachment" content="true">
    <meta name="google-site-verification" content="{{config('google.verification')}}">
    <meta name="msvalidate.01" content="{{config('bing.verification')}}">
    <!-- FACEBOOK OPEN GRAPH -->
    <meta property="fb:app_id" content="{{config('facebook.id')}}">
    <meta property="article:publisher" content="{{config('facebook.url')}}">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="{{config('app.description')}}">
    <meta name="twitter:title" content="@yield('title'){{config('app.title')}}">
    <meta name="twitter:image" content="{{asset(config('app.banner').'?v=4.1')}}">
    <meta name="author" content="giasuthanhdanh.com, Nguyễn Linh - ntt.nguyenlinh@gmail.com">
    <meta name="copyright" content="Copyright 2019 - {{config('app.name')}}">
    <link rel='shortlink' href='{{config('app.url')}}'>

    <link rel="icon" href="{{asset(config('app.icon').'?v=4.1')}}" type="image/x-icon">
    <link rel="apple-touch-icon image_src" href="{{asset(config('app.icon').'?v=4.1')}}">

    <link rel="stylesheet" href="{{asset('css/style.css?v=4.1')}}" media="screen,print">
    <link rel="stylesheet" href="{{asset('font-awesome/css/all.min.css?v=4.1')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css?v=4.1')}}">
    <link rel="stylesheet" href="{{asset('css/faq.css?v=4.1')}}">
    <link rel="stylesheet" href="{{asset('css/sweetalert2.min.css?v=4.1')}}">
</head>
<body>
    <div id="background"></div>
    <div class="wrapper">
        <div class="header">
            <img class="logo" src="{{asset('images/logo.png')}}">
            <ul class="menu clearfix">
                <li class="m1">
                    <a href="{{route('home')}}" title="{{config('app.title')}}">TRANG CHỦ</a>
                </li>
                <li class="m2">
                    <a href="{{route('dang-ky-tim-gia-su.index')}}" title="Đăng ký tìm gia sư">ĐĂNG KÝ TÌM GIA SƯ</a>
                </li>

                <li class="m5"></li>

                <li class="m3">
                    <a href="{{route('danhsachlop')}}" title="Lớp hiện có">LỚP HIỆN CÓ</a>
                </li>

                <li class="m4">
                    <a href="{{route('noiquy')}}" title="Nội quy nhận lớp">NỘI QUY NHẬN LỚP</a>
                </li>
            </ul>

            <ul class="menu clearfix" style="margin-top:-4px; padding-top:0;">
                <li class="m1">
                    <a href="{{route('tailieu')}}" title="tài liệu học tập">TÀI LIỆU HỌC TẬP</a>
                </li>
                <li class="m2">
                    <a href="{{route('hocphi')}}" title="Học phí tham khảo">HỌC PHÍ THAM KHẢO</a>
                </li>

                <li class="m5"></li>

                <li class="m3">
                    <a href="{{route('dang-ky-lam-gia-su.index')}}" title="Đăng ký thành viên">ĐĂNG KÝ THÀNH VIÊN</a>
                </li>

                <li class="m4">
                    <a href="{{route('lienhe')}}" title="Thông tin liên hệ">THÔNG TIN LIÊN HỆ</a>
                </li>
            </ul>

        </div>
        <div class="slideOut">
            <div id="slider" class="swipe">
                <div class="swipe-wrap">
                    <div>
                        <img src="/thumb.php?src={{asset('images/gia-su-thanh-danh-day-kem-tai-nha-1.jpg?v=4.1')}}"/>
                    </div>
                    <div>
                        <img src="/thumb.php?src={{asset('images/gia-su-thanh-danh-day-kem-tai-nha-2.jpg?v=4.1')}}"/>
                    </div>
                    <div>
                        <img src="/thumb.php?src={{asset('images/gia-su-thanh-danh-day-kem-tai-nha-3.jpg?v=4.1')}}"/>
                    </div>
                    <div>
                        <img src="/thumb.php?src={{asset('images/gia-su-thanh-danh-day-kem-tai-nha-4.jpg?v=4.1')}}"/>
                    </div>
                    <div>
                        <img src="/thumb.php?src={{asset('images/gia-su-thanh-danh-day-kem-tai-nha-5.jpg?v=4.1')}}"/>
                    </div>
                </div>
            </div>
            <p class="slide_top">
                <img src="/thumb.php?src={{asset('images/gia-su-thanh-danh-day-kem-tai-nha.png?v=4.1')}}"/>
            </p>
            <p class="slide_bottom">
                <img src="/thumb.php?src={{asset('images/slide-bottom.jpg?v=4.1')}}"/>
            </p>
            <div class="slide_left">
                <p style="font-size: 14pt; font-weight:900; text-align: center; color:#1c87c9;">
                    Phụ huynh - học viên
                    <span style="text-align: center; font-size: 13pt;" class="blink-two">Tìm gia sư</span>
                </p>
                <p style="font-size: 13pt; margin-top: 5pt; margin-left:-10px; color:red;" class="blink-two">{{config('app.sdt1')}}</p>
            </div>
            <div class="slide_right">
                <p style="font-size: 14pt; font-weight:900; text-align: center; color:#1c87c9; margin-left:50px;">
                    Giáo viên - sinh viên
                    <span class="blink-two" style="text-align: center; font-size: 13pt;">Tìm lớp dạy</span>
                </p>
                <p class="blink-two" style="font-size: 13pt; margin-top: 5pt; color:red; margin-right: -30px;">{{config('app.sdt2')}}</p>

                <div style="margin-top:20px; margin-right:-60px;">
                    <p style="text-align:center; padding:10px 30px; font-size: 16pt; font-style: oblique; color:rgba(255, 0, 255, 0.71);">
                        GIA SƯ CỦA SỰ
                    </p>
                    <p style="text-align:left; padding:10px 30px; font-size: 14pt; font-style: oblique; color:#80ffff;">
                        <blink class="blink-two"><i class="fal fa-check"></i> CHUYÊN NGHIỆP</blink>
                    </p>
                    <p style="text-align:left; padding:10px 30px; font-size: 14pt; font-style: oblique; color:#80ffff;">
                        <blink class="blink-two"><i class="fal fa-check"></i> TẬN TÂM</blink>
                    </p>
                    <p style="text-align:left; padding:10px 30px; font-size: 14pt; font-style: oblique; color:#80ffff;">
                        <blink class="blink-two"><i class="fal fa-check"></i> TRÁCH NHIỆM</blink>
                    </p>
                    <p style="text-align:left; padding:10px 30px; font-size: 14pt; font-style: oblique; color:#80ffff;">
                        <blink class="blink-two"><i class="fal fa-check"></i> HÀI LÒNG</blink>
                    </p>
                    <p style="text-align:left; padding:10px 30px; font-size: 14pt; font-style: oblique; color:#80ffff;">
                        <blink class="blink-two"><i class="fal fa-check"></i> VỮNG VÀNG KIẾN THỨC</blink>
                    </p>
                </div>
            </div>
        </div>
