@extends('layouts.admin')
@section('title', 'Cấu hình Facebook Messenger - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li><a href="{{Route('admin.tools')}}">Cài đặt</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.tools.chat')}}">Cấu hình Facebook Messenger</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight:bold;">
                        <i class="fas fa-cogs"></i>
                        Cấu hình Facebook Messenger
                    </h3>
                </div>
                <div class="col-md-12" style="float: none; display: block; margin: 0 auto;">
                    <div class="well alert-block alert-warning">
                        <i class="ace-icon fal fa-exclamation-triangle"></i>
                        Tham khảo cách cấu hình Facebook Messenger <a href="{{asset('storage/documents/H%C6%B0%E1%BB%9Bng%20d%E1%BA%ABn%20t%C3%ADch%20h%E1%BB%A3p%20Facebook%20Messenger%20v%C3%A0o%20Website.pdf')}}">Hướng dẫn tích hợp Facebook Messenger vào Website</a>
                    </div>
                    <form method="post" action="{{route('admin.tools.store')}}">
                        @csrf
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Facebook Page URL</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="fburl" placeholder="Facebook Page URL"
                                   value="{{config('facebook.url')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Facebook Page ID</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="fbid" placeholder="Facebook Page ID"
                                   value="{{config('facebook.id')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Lời chào</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="fbgreeting" placeholder="Lời chào"
                                   value="{{config('facebook.greeting')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Màu hiển thị</p>
                            <input style="border: #057bbe 1px solid; color: #057bbe; width:10%;"
                                   type="text" id="fbcolor" name="fbcolor" placeholder="Màu hiển thị"
                                   value="{{config('facebook.color')}}"/>
                            <input type='text' id="color"/>
                        </div>
                        <br>
                        <div class="center">
                            <input type="submit" name="chat" class="btn btn-sm btn-primary" value="Thiết lập"/>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <link rel="stylesheet" href="{{asset('adm/css/tools.css?v=4.9')}}"/>
    <link rel="stylesheet" href="{{asset('spectrum/spectrum.css?v=4.9')}}">
    <script src="{{asset('spectrum/spectrum.js?v=4.9')}}"></script>
    <script src="{{asset('adm/js/chat.js?v=4.9')}}"></script>
@endsection

