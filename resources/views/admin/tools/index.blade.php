@extends('layouts.admin')
@section('title', 'Cài đặt chung - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li><a href="{{Route('admin.tools')}}">Cài đặt</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.tools')}}">Cài đặt chung</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight:bold;">
                        <i class="fas fa-cogs"></i>
                        Cài đặt chung
                    </h3>
                </div>
                <form method="post" action="{{route('admin.tools.store')}}">
                    @csrf
                    <div class="widget-main">
                        <div>
                           <h3 class="row header smaller lighter blue">
							    <span class="col-sm-7">
							        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="width:25px; height:25px;">
                                      <path fill="currentColor" d="M146.27 303.84l30.88-4.41a79.07 79.07 0 0 0 6.77 22.72l-24.8 16.54a16 16 0 1 0 17.75 26.62l26.39-17.59a76.88 76.88 0 0 0 71.68 17.73L176 266.5v.76l-34.27 4.9a16 16 0 0 0 4.54 31.68zM256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 432c-101.46 0-184-82.54-184-184a182.89 182.89 0 0 1 33.38-105.37l256 256A182.89 182.89 0 0 1 256 440zm150.62-78.63l-60.3-60.3 19.41 2.77a16.23 16.23 0 0 0 2.27.16 16 16 0 0 0 2.25-31.84l-34.25-4.9v-22.52l34.27-4.9a16 16 0 1 0-4.54-31.68l-30.88 4.41a79.07 79.07 0 0 0-6.77-22.72l24.79-16.54a16 16 0 1 0-17.75-26.62l-26.38 17.59a78.43 78.43 0 0 0-102-2.79l-56.11-56.11A182.89 182.89 0 0 1 256 72c101.46 0 184 82.54 184 184a182.89 182.89 0 0 1-33.38 105.37z"></path>
                                   </svg>
                                    <span>Kích hoạt Debug</span>
                                </span>
                                <span class="col-sm-5">
						            <label class="pull-right inline">
                                        @if(config('app.debug') == 'true')
                                            <input name="debug" type="checkbox" class="ace ace-switch ace-switch-5" checked>
                                        @else
                                            <input name="debug" type="checkbox" class="ace ace-switch ace-switch-5">
                                       @endif
                                        <span class="lbl middle"></span>
								    </label>
							    </span>
                            </h3>
                       </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Tên trang</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="tentrang" placeholder="Tên trang"
                                   value="{{config('app.name')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Khẩu hiệu</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="khauhieu" placeholder="Khẩu hiệu"
                                   value="{{config('app.subname')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Tiêu đề trang</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="tieude" placeholder="Tiêu đề"
                                   value="{{config('app.title')}}" required/>
                        </div>
                        <div style="margin-top: 20px;">
                            <span class="bolder">Biểu tượng cho trang (.ico hoặc .png 128x128)</span>
                            <input class="form-control" type="hidden" name="bieutuong" id="bieutuong"
                                   value="" required/>
                            <center>
                                <img src="{{asset(config('app.icon').'?v=4.9')}}" id="imgicon"/>
                            </center>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Địa chỉ trang (URL)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="url" placeholder="Địa chỉ trang"
                                   value="{{config('app.url')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Google Verification</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="googleverification" placeholder="Google Verification"
                                   value="{{config('google.verification')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Bing Verification</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="bingverification" placeholder="Bing Verification"
                                   value="{{config('bing.verification')}}" required/>
                        </div>
                    </div>
                    <br>
                    <div class="center">
                        <input type="submit" name="thongtintrang" class="btn btn-sm btn-primary" value="Thay đổi"/>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <link rel="stylesheet" href="{{asset('adm/css/tools.css?v=4.9')}}"/>
    <script src="{{asset('adm/js/seo.js?v=4.9')}}"></script>
@endsection

