@extends('layouts.admin')
@section('title', 'Sơ đồ trang - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li><a href="{{route('admin.tools')}}">Cài đặt</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.sitemap')}}">Sơ đồ trang</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight:bold;">
                        <i class="fas fa-cogs"></i>
                        Sơ đồ trang
                    </h3>
                </div>
                <div class="panel-body">
                    <p style="text-indent: 20px; text-align:justify; line-height:1.5;">
                        Sitemap hay còn gọi là sơ đồ trang web là một bản đồ thu nhỏ tập hợp các đường dẫn URL đang có trên một trang web, hỗ trợ điều hướng hành vi của người dùng trên website đồng thời còn giúp cho con Bot của Google dễ thu thập dữ liệu và đánh chỉ mục website.
                    </p>
                    <hr>

                    <div class="col-sm-12">
                        <div class="widget-box">
                            <div class="widget-header" style="background: #2283c5; border-radius: 10px;">
                                <h4 class="widget-title">Chức năng của công cụ này</h4>
                            </div>
                        </div>

                        <ul class="list-unstyled spaced" style="padding:10px;">
                            <li>
                                <i class="ace-icon fa fa-check bigger-110 green"></i>
                                Tạo kịch bản XML sơ đồ trang web để gửi tới Google, Bing, các công cụ tìm kiếm khác để thu thập dữ liệu trang web.
                            </li>

                            <li>
                                <i class="ace-icon fa fa-check bigger-110 green"></i>
                                Tạo kịch bản XML đơn giản để tập hợp tất cả các trang.
                            </li>

                            <li>
                                <i class="ace-icon fa fa-check bigger-110 green"></i>
                                Tạo ra đường dẫn /sitemap.xml và lưu nó trên trang web.
                            </li>

                        </ul>

						<div class="widget-box">
                            <div class="widget-header" style="background: #2283c5; border-radius: 10px;">
                                <h4 class="widget-title">Các bước thực hiện</h4>
                            </div>
                        </div>

                        <ul class="list-unstyled spaced" style="padding:10px;">
                            <li>
                               1 - Bấm nút tạo sitemap và chờ đến khi thanh tiến trình đầy cây. Sau đó tải lại trang.
                            </li>

                            <li>
                                2 - Bấm nút chỉnh sửa trong khung view nhanh bên dưới. Sửa <priority>1</priority> cho dòng <url> đầu tiên.
                            </li>

                            <li>
                                3 - Tìm và xoá hết những dòng <url> có dạng /thumb.php .......
                            </li>
							<li>
                                4 - Tìm và xoá hết những dòng <url> giả demo không phải dữ liệu thật.
                            </li>
							<li>
                                5 - Cuối cùng bấm nút xong.
                            </li>
							<li>
                                6 - Đăng nhập vào trang <a href="https://search.google.com/search-console">https://search.google.com/search-console</a> với tài khoản gmail sở hữu trang web.
                            </li>
							<li>
                                7 - Bấm vào dấu 3 gạch trên cùng bên trái chọn tiếp Tìm kiếm sản phẩm. Chọn vào trang web cần đăng sitemap.
                            </li>
							<li>
                                8 - Chọn tiếp sơ đồ trang web. Trong khung thêm mới sitemap nhập vô là sitemap.xml rồi bấm gửi.
                            </li>
                        </ul>
						
                        <p style="text-indent: 20px; text-align:center; line-height:1.5;">
                            <button id="sitemap-btn" type="button" class="btn btn-primary" data-loading-text="Đang tạo...">Tạo sitemap</button>
                        </p>
                        <div class="progress progress-striped active" style="display:none;">
                            <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="widget-box">
                            <div class="widget-header" style="background: #2283c5; border-radius: 10px;">
                                <h4 class="widget-title">Nội dung sitemap.xml
                                    <span style="float:right; margin-right:10px;">
                                        <button class="label label-info label-white middle" id="btn-edit">Chỉnh sửa</button>
                                        <button class="label label-success label-white middle" id="btn-done" style="background-color: #eaf3e5!important; display:none;">Xong</button>
                                    </span>
                                </h4>
                            </div>
                            <div class="form-group">
                              <textarea id="txtsitemap" name="txtsitemap" class="form-control col-xs-12" cols="100" readonly="true">{!! $sitemap !!}</textarea>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <style>
      .form-control {min-width:100% !important; width:100% !important;}
        textarea{outline: none; overflow: auto; min-width:100% !important; width:100% !important; height:500px !important; resize: vertical !important;}
        textarea:focus{outline: none; overflow: auto;border: 1px solid #D5D5D5!important;}
    </style>
    <script src="{{asset('adm/js/vkbeautify.js')}}"></script>
    <script src="{{asset('adm/js/sitemap.js')}}"></script>
@endsection
