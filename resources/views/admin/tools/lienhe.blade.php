@extends('layouts.admin')
@section('title', 'Thay đổi thông tin liên hệ - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li><a href="{{Route('admin.tools')}}">Cài đặt</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.tools.lienhe')}}">Thay đổi thông tin liên hệ</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight:bold;">
                        <i class="fas fa-cogs"></i>
                        Thay đổi thông tin liên hệ
                    </h3>
                </div>
                <form method="post" action="{{route('admin.tools.store')}}">
                    @csrf
                    <div class="widget-main">
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Địa chỉ Email</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="email" name="email" placeholder="Email"
                                   value="{{config('app.email')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Số ĐT 1</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="sdt1" placeholder="Số ĐT 1"
                                   value="{{config('app.sdt1')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Số ĐT 2</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="sdt2" placeholder="Số ĐT 2"
                                   value="{{config('app.sdt2')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Văn phòng 1</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="diachi11" placeholder="Văn phòng 1"
                                   value="{{config('app.diachi11')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Địa chỉ văn phòng 1</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="diachi1" placeholder="Địa chỉ văn phòng 1"
                                   value="{{config('app.diachi1')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Văn phòng 2</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="diachi22" placeholder="Văn phòng 2"
                                   value="{{config('app.diachi22')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Địa chỉ văn phòng 2</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;"
                                   type="text" name="diachi2" placeholder="Địa chỉ văn phòng 2"
                                   value="{{config('app.diachi2')}}" required/>
                        </div>
                    </div>
                    <br>
                    <div class="center">
                        <input type="submit" name="lienhe" class="btn btn-sm btn-primary" value="Thay đổi"/>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <link rel="stylesheet" href="{{asset('adm/css/tools.css?v=4.9')}}"/>
    <script src="{{asset('adm/js/seo.js?v=4.9')}}"></script>
@endsection

