@extends('layouts.admin')
@section('title', 'Cài đặt SEO - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li><a href="{{Route('admin.tools')}}">Cài đặt</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.tools.seo')}}">Cài đặt SEO</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight:bold;">
                        <i class="fas fa-cogs"></i>
                        Cài đặt SEO
                    </h3>
                </div>
                <div class="widget-main">
                    <form method="post" action="{{route('admin.tools.store')}}">
                        @csrf
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Mô tả (Description)</p>
                            <textarea class="form-control" style="border: #057bbe 1px solid; height:100px; color: #057bbe; resize: none;" id="mota" name="mota" placeholder="Mô tả" required autofocus>{{config('app.description')}}</textarea>
                            <div id="the-count">
                                <span id="current">0</span>
                                <span id="maximum">/ 300</span>
                            </div>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Từ khoá (Keywords)</p>
                            <textarea class="form-control" style="border: #057bbe 1px solid; height:100px; color: #057bbe; resize: none;" id="tukhoa" name="tukhoa" placeholder="Từ khoá" required>{{config('app.keywords')}}</textarea>
                        </div>
                        <div style="margin-top: 20px;">
                            <span class="bolder">Banner (.png 1200x630)</span>
                            <input class="form-control" type="hidden" name="banner" id="bieutuong" value="" required/>
                            <center>
                                <img src="{{asset(config('app.banner').'?v=4.9')}}" style="width:70%; height:50%;" id="imgicon"/>
                            </center>
                        </div>
                        <br>
                        <div class="center">
                            <input type="submit" name="seo" class="btn btn-sm btn-primary" value="Thay đổi"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <link rel="stylesheet" href="{{asset('adm/css/tools.css?v=4.9')}}"/>
    <script src="{{asset('adm/js/seo.js?v=4.9')}}"></script>
@endsection

