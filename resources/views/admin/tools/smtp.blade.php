@extends('layouts.admin')
@section('title', 'Cấu hình SMTP - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li><a href="{{Route('admin.tools')}}">Cài đặt</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.tools.smtp')}}">Cấu hình SMTP</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight:bold;">
                        <i class="fas fa-cogs"></i>
                        Cấu hình SMTP
                    </h3>
                </div>
                <div class="col-md-12" style="float: none; display: block; margin: 0 auto;">
                    <div class="well alert-block alert-warning">
                        <i class="ace-icon fal fa-exclamation-triangle"></i>
                        Tham khảo cách thiết lập SMTP cho GMAIL <a href="{{asset('storage/documents/huong_dan_xac_thuc_2_lop_gmail.pdf')}}">Hướng dẫn xác thực 2 lớp</a>, <a href="{{asset('storage/documents/Huong_dan_tao_mat_khau_ung_dung_gmail.pdf')}}">Hướng dẫn tạo mật khẩu ứng dụng</a>
                    </div>
                    <form method="post" action="{{route('admin.tools.store')}}">
                        @csrf
                        <div>
                            <h3 class="row header smaller lighter blue">
							    <span class="col-sm-7">
							        <i class="fas fa-paper-plane"></i>
                                    <span>Kích hoạt gửi MAIL</span>
                                </span>
                                <span class="col-sm-5">
						            <label class="pull-right inline">
                                        @if(config('mail.turnon') == 'on')
                                            <input name="turnon" type="checkbox" class="ace ace-switch ace-switch-5" checked>
                                        @else
                                            <input name="turnon" type="checkbox" class="ace ace-switch ace-switch-5">
                                        @endif
                                        <span class="lbl middle"></span>
								    </label>
							    </span>
                            </h3>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Loại cấu hình (Driver)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="DRIVER" placeholder="Loại cấu hình" class="in_405" value="{{config('mail.driver')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Máy chủ gửi đi (Host)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="HOST" placeholder="Máy chủ gửi đi" class="in_405" value="{{config('mail.host')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Cổng gửi mail (Port)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="PORT" placeholder="Cổng gửi mail" class="in_405" value="{{config('mail.port')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Tên tài khoản (Username)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="USERNAME" placeholder="Tên tài khoản" class="in_405" value="{{config('mail.username')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Mật khẩu (Password)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="PASSWORD" placeholder="Mật khẩu" class="in_405" value="{{config('mail.password')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Kiểu xác thực (Encryption)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="ENCRYPTION" placeholder="Kiểu xác thực" class="in_405" value="{{config('mail.encryption')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Email gửi đi (From Address)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="FROM_ADDRESS" placeholder="nhập email đang dùng để gửi" class="in_405" value="{{config('mail.from.address')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Tên người gửi (From Name)</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="FROM_NAME" placeholder="Tên người gửi, khuyến khích lấy tên website" class="in_405" value="{{config('mail.from.name')}}" required/>
                        </div>
                        <div>
                            <p class="bolder" style="padding-top: 20px;">Email Admin</p>
                            <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" name="ADMIN_ADDRESS" placeholder="Địa chỉ email của Admin để nhận email" class="in_405" value="{{config('mail.admin_address')}}" required/>
                        </div>
                        <br>
                        <div class="center">
                            <input type="submit" name="cauhinhmail" class="btn btn-sm btn-primary" value="Thay đổi"/>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <link rel="stylesheet" href="{{asset('adm/css/tools.css')}}"/>
@endsection

