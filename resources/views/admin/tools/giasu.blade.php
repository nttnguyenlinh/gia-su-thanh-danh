@extends('layouts.admin')
@section('title', 'Thay đổi thông tin Gia Sư - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li><a href="{{Route('admin.tools')}}">Cài đặt</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.tools.giasu')}}">Thay đổi thông tin Gia Sư</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight:bold;">
                        <i class="fas fa-cogs"></i>
                        Thay đổi thông tin gia sư
                    </h3>
                </div>

                <div class="col-md-12" style="float: none; display: block; margin: 0 auto;">
                    <div class="widget-box widget-color-blue">
                        <div class="widget-header">
                            <h4 class="widget-title">Thay Đổi thông tin hồ sơ</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <form method="post" id="frmToolsSDT" action="{{route('admin.tools.giasu.store')}}">
                                    @csrf
                                    <div>
                                        <p class="bolder">Thay đổi cho</p>
                                        <select class="chosen-select" id="chosen-id" name="id" required>
                                            <option value=""></option>
                                            @foreach($giasu as $row)
                                                <option value="{{$row->id}}">{{$row->id}} - {{$row->holot}} {{$row->ten}}</option>
                                            @endforeach
                                        </select>

                                        <p class="bolder" style="padding-top: 20px;">Họ lót</p>
                                        <div>
                                            <input class="form-control in_405" style="border: #057bbe 1px solid; color: #057bbe; width:100%;" type="text" id="holot" name="holot" placeholder="" maxlength="30" required/>
                                        </div>

                                        <p class="bolder" style="padding-top: 20px;">Tên</p>
                                        <div>
                                            <input class="form-control in_405" style="border: #057bbe 1px solid; color: #057bbe; width:100%;" type="text" id="ten" name="ten" placeholder="" maxlength="30" required/>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="center">
                                        <input type="submit" name="thaydoihoso" class="btn btn-sm btn-primary" value="Thay đổi"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="float: none; display: block; margin: 0 auto;">
                    <div class="widget-box widget-color-blue">
                        <div class="widget-header">
                            <h4 class="widget-title">Thay Đổi SĐT</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <form method="post" id="frmToolsSDT" action="{{route('admin.tools.giasu.store')}}">
                                    @csrf
                                    <div>
                                        <p class="bolder">Thay đổi cho</p>
                                        <select class="chosen-select" id="chosen-idSDT" name="id" required>
                                            <option value=""></option>
                                            @foreach($giasu as $row)
                                                <option value="{{$row->id}}">{{$row->id}} - {{$row->holot}} {{$row->ten}}</option>
                                            @endforeach
                                        </select>

                                        <p class="bolder" style="padding-top: 20px;">Nhập số điện thoại mới</p>
                                        <div class="kiemtraSDT">
                                            <div class="input-group">
                                                <input class="form-control in_405" style="border: #057bbe 1px solid; color: #057bbe;" type="text" id="sdt" name="sdt" placeholder="" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                                                <span class="input-group-addon" style="border: #057bbe 1px solid; color: #057bbe;"><i class="ace-icon fal fa-phone"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="center">
                                        <input type="submit" name="thaydoiSDT" class="btn btn-sm btn-primary" value="Thay đổi"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="float: none; display: block; margin: 0 auto;">
                    <div class="widget-box widget-color-blue">
                        <div class="widget-header">
                            <h4 class="widget-title">Thay Đổi Email</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <form method="post" id="frmToolsEmail" action="{{route('admin.tools.giasu.store')}}">
                                    @csrf
                                    <div>
                                        <p class="bolder">Thay đổi cho</p>
                                        <select class="chosen-select" id="chosen-idEMAIL" name="id" required>
                                            <option value=""></option>
                                            @foreach($giasu as $row)
                                                <option value="{{$row->id}}">{{$row->id}} - {{$row->holot}} {{$row->ten}}</option>
                                            @endforeach
                                        </select>

                                        <p class="bolder" style="padding-top: 20px;">Nhập địa chỉ Email mới</p>
                                        <div class="kiemtraEMAIL">
                                            <div class="input-group">
                                                <input class="form-control in_405" style="border: #057bbe 1px solid; color: #057bbe;" type="text" id="email" name="email" placeholder="" maxlength="50" required/>
                                                <span class="input-group-addon" style="border: #057bbe 1px solid; color: #057bbe;"><i class="ace-icon fal fa-at"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="center">
                                        <input type="submit" name="thaydoiEmail" class="btn btn-sm btn-primary" value="Thay đổi"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="float: none; display: block; margin: 0 auto;">
                    <div class="widget-box widget-color-blue">
                        <div class="widget-header">
                            <h4 class="widget-title">Thay Đổi CMND/CCCD</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <form method="post" id="frmToolsCMND" action="{{route('admin.tools.giasu.store')}}">
                                    @csrf
                                    <div>
                                        <p class="bolder">Thay đổi cho</p>
                                        <select class="chosen-select" id="chosen-idCMND" name="id" required>
                                            <option value=""></option>
                                            @foreach($giasu as $row)
                                                <option value="{{$row->id}}">{{$row->id}} - {{$row->holot}} {{$row->ten}}</option>
                                            @endforeach
                                        </select>

                                        <p class="bolder" style="padding-top: 20px;">Nhập số CMND/CCCD mới</p>
                                        <div class="kiemtraCMND">
                                            <div class="input-group">
                                                <input class="form-control in_405" style="border: #057bbe 1px solid; color: #057bbe;" type="text" id="cmnd" name="cmnd" placeholder="" maxlength="12" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                                                <span class="input-group-addon" style="border: #057bbe 1px solid; color: #057bbe;"><i class="ace-icon fal fa-portrait"></i></span>
                                            </div>
                                        </div>
                                        <p class="bolder" style="padding-top: 20px;">Upload ảnh mặt trước CMND/CCCD</p>
                                        <div>
                                            <label class="ace-file-input">
                                                    <span class="ace-file-container" id="choose_cmnd" data-title="Chọn">
                                                        <input type="hidden" name="cmndName" id="cmndName" class="form-control input-sm" value="" >
                                                        <span class="ace-file-name" id="fileName" data-title="Chưa chọn ảnh"></span>
                                                    </span>
                                                <a class="remove" href="#"><i class="ace-icon fal fa-times"></i></a>
                                            </label>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="center">
                                        <input type="submit" name="thaydoiCMND" class="btn btn-sm btn-primary" value="Thay đổi"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="float: none; display: block; margin: 0 auto;">
                    <div class="widget-box widget-color-blue">
                        <div class="widget-header">
                            <h4 class="widget-title">Cấp lại mật khẩu</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <form method="post" id="frmToolsMK" action="{{route('admin.tools.giasu.store')}}">
                                    @csrf
                                    <div>
                                        <p class="bolder">Cấp lại mật khẩu cho</p>
                                        <select class="chosen-select" id="chosen-idMK" name="id" required>
                                            <option value=""></option>
                                            @foreach($giasu as $row)
                                                <option value="{{$row->id}}">{{$row->id}} - {{$row->holot}} {{$row->ten}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <br>
                                    <div class="center">
                                        <input type="submit" name="CapLaiMK" class="btn btn-sm btn-primary" value="Cấp lại"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <style>
        .col-md-12{min-width:100% !important; width:100% !important;}
    </style>
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('adm/css/tools.css')}}"/>
    <script src="{{asset('chosen/chosen.jquery.min.js')}}"></script>
    <script src="{{asset('adm/js/tools.js')}}"></script>
@endsection

