@extends('layouts.admin')
@section('title', 'Lưu trữ - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.storage')}}">Lưu trữ</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div id="storage"></div>
        </div>
    </div>
@endsection

@section('footer')
    <style>
        .col-md-12{min-width: 100%!important; padding:0!important;}
    </style>
    <script>
        CKFinder.widget('storage', {
            resourceType: 'Storage',
            height: '650',
            plugins: ['plugins/StatusBarInfo/StatusBarInfo']
        });
    </script>
@endsection
