@extends('layouts.admin')
@section('title', 'Bài viết - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li style="font-weight:bold;"><a href="{{Route('admin.baiviet')}}">Bài viết</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <a href="{{route('admin.baiviet_them')}}" class="btn btn-default" style="float:right;">
        <i class="ace-con fal fa-file-plus"></i> Thêm bài viết
    </a>

    <div class="clearfix" style="margin-bottom: 10px;"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title" style="font-weight:bold;">
                        <i class="fas fa-atom-alt"></i>
                        Danh sách bài viết
                    </h3>
                </div>
                <table id="tb_baiviet" class="table table-striped table-hover table-bordered table-responsive" style="width:100%">
                    <thead>
                    <tr>
                        <th width="20">ID</th>
                        <th width="50">Danh mục</th>
                        <th>Tiêu đề</th>
                        <th>Mô tả</th>
                        <th width="50"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" style="width:30px; height:20px;"><path fill="currentColor" d="M464 448H48c-26.51 0-48-21.49-48-48V112c0-26.51 21.49-48 48-48h416c26.51 0 48 21.49 48 48v288c0 26.51-21.49 48-48 48zM112 120c-30.928 0-56 25.072-56 56s25.072 56 56 56 56-25.072 56-56-25.072-56-56-56zM64 384h384V272l-87.515-87.515c-4.686-4.686-12.284-4.686-16.971 0L208 320l-55.515-55.515c-4.686-4.686-12.284-4.686-16.971 0L64 336v48z"></path></svg></th>
                        <th width="60">Trạng thái</th>
                        <th width="80">#</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <link rel="stylesheet" href="{{asset('adm/css/baiviet.css?v=4.9')}}"/>
    <script src="{{asset('adm/js/baiviet.js?v=4.9')}}"></script>
@endsection

