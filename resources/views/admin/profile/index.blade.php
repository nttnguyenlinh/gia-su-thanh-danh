@extends('layouts.admin')
@section('title', 'Cá nhân - ')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li><a href="{{route('admin')}}">Dashboard</a></li>
            <li style="font-weight:bold;"><a href="{{route('admin.profile')}}">Cá nhân</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div id="user-profile-1" class="user-profile row">
                <div class="col-xs-12 col-sm-3 center" style="margin-top: 30px;">
                    <span class="profile-picture">
                        @if(!empty(Auth::user()->avatar))
                            <a href="/thumb.php?src={{asset('storage/anhthe/'.Auth::user()->avatar . '?v=4.9')}}&w=300&h=400&p=100" data-lightbox="image">
                                <img src="/thumb.php?src={{asset('storage/anhthe/'.Auth::user()->avatar . '?v=4.9')}}&w=300&h=400&p=100" class="editable img-responsive-cus"/>
                            </a>
                        @else
                            <img src="/thumb.php?src={{asset('storage/anhthe/no_image.jpg?v=4.9')}}&w=300&h=400&p=100" class="editable img-responsive-cus"/>
                        @endif
                    </span>

                    <div class="space-4"></div>

                    @if(Auth::user()->is_active)
                        <div class="width-50 label label-info label-xlg arrowed-in arrowed-in-right">
                            <i class="ace-icon fa fa-circle light-green"></i>
                            <span class="white">Hoạt động</span>
                        </div>
                    @else
                        <div class="width-50 label label-danger label-xlg arrowed-in arrowed-in-right">
                            <i class="ace-icon fa fa-circle light-red"></i>
                            <span class="white">Bị khoá</span>
                        </div>
                    @endif

                    <div class="space-16"></div>
                </div>

                <div class="col-xs-12 col-sm-9">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-flat">
                            <h4 class="widget-title blue smaller">
                                <i class="ace-icon fal fa-user blue"></i>
                                Thông tin cá nhân
                            </h4>

                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <div class="space-10"></div>
                                <form method="post" id="form_profile_update" action="{{Route('admin.profile_store')}}" enctype="multipart/form-data">
                                   @csrf
                                    <input type="hidden" name="id" class="form-control input-sm" value="{{Auth::user()->id}}">
                                    <div class="profile-user-info profile-user-info-striped">
                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Họ tên</div>
                                            <div class="profile-info-value">
                                                <input type="text" name="name" id="name" class="form-control input-sm" value="{{Auth::user()->name}}"  required maxlength="30">
                                            </div>
                                        </div>

                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Email</div>
                                            <div class="profile-info-value kiemtraEMAIL">
                                                <input type="email" name="email" id="email" class="form-control input-sm" value="{{Auth::user()->email}}" onpaste="onpaste()" required>
                                            </div>
                                        </div>

                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Hình đại diện</div>
                                            <div class="profile-info-value">
                                                <label class="ace-file-input">
                                                    <span class="ace-file-container" id="choose_avatar" data-title="Chọn">
                                                        <input type="hidden" name="avatarName" id="avatarName" class="form-control input-sm" value="" >
                                                        <span class="ace-file-name" id="fileName" data-title="Chưa chọn ảnh"></span>
                                                    </span>
                                                    <a class="remove" href="#"><i class="ace-icon fal fa-times"></i></a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>

                                    <div class="col-md-offset-3 col-md-6" style="text-align: center;">
                                        <button class="btn btn-info" type="submit" name="Update">
                                            <i class="ace-icon fa fa-check bigger-110"></i>Cập nhật
                                        </button>
                                        <button class="btn" type="reset">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>Clear
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-9" style="float:right; margin-top: 20px;">
                <div class="widget-box transparent">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title blue smaller">
                            <i class="ace-icon fal fa-key blue"></i>
                            Thay đổi mật khẩu
                        </h4>

                        <div class="widget-toolbar">
                            <a href="javascript:void(0);" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main no-padding">
                            <div class="space-10"></div>
                            <form method="post" id="form_profile_password" action="{{route('admin.profile_changePassword')}}">
                                @csrf
                                <input type="hidden" name="id" class="form-control input-sm" value="{{Auth::user()->id}}">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">M.khẩu hiện tại</div>
                                        <div class="profile-info-value">
                                            <input type="password" name="current_password" id="current_password" class="form-control input-sm" required>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Mật khẩu mới</div>
                                        <div class="profile-info-value">
                                            <input type="password" name="new_password" id="new_password" class="form-control input-sm" required>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Xác nhận M.k</div>
                                        <div class="profile-info-value">
                                            <input type="password" name="confirm_password" id="confirm_password" class="form-control input-sm" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="col-md-offset-3 col-md-6 text-center">
                                    <button class="btn btn-info" type="submit" name="changePass">
                                        <i class="ace-icon fa fa-check bigger-110"></i>Thay đổi
                                    </button>
                                    <button class="btn" type="reset">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>Clear
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <link href="{{asset('lightbox/dist/css/lightbox.css')}}" rel="stylesheet" />
    <script src="{{asset('lightbox/dist/js/lightbox.js')}}"></script>
    <script src="{{asset('adm/js/profile.js')}}"></script>
@endsection

