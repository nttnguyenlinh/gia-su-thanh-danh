@extends('layouts.home')
@section('title', 'Danh sách lớp cần gia sư - ')
@section('canonical', Route('danhsachlop'))
@section('url', Route('danhsachlop'))
@section('content')
    <div class="pageBody clearfix">
        <div class="pageBodyInner clearfix">
            <div class="col_c">
                <div class="newClass">
                    <div class="dsgsdk">
                        <h1>DANH SÁCH LỚP - CẬP NHẬT NGÀY {{date("d-m-Y")}}</h1>
                    </div>
                    <div class="search clearfix">
                        <form method="post" action="{{route('timkiemlop')}}">
                            @csrf
                            <input type="text" name="tukhoa" placeholder="Nhập mã lớp hoặc tên môn dạy" class="in_search" style="color: #008DCA; border:#008DCA 1px solid; width:40%; float:left; margin-right:4px;" autofocus>

                            <select name="quanhuyen" style="margin-right:4px; color:#008DCA; cursor: pointer;">
                                <option value="" disabled selected>Quận/huyện</option>
                                @foreach($khuvuc as $kv)
                                    <option value="{{$kv->id}}">{{$kv->tenkv}}</option>
                                @endforeach
                            </select>

                            <select name="lopday" style="width:100%; color:#008DCA; cursor: pointer;">
                                <option value="" disabled selected>Lớp dạy</option>
                                @foreach($lophoc as $lop)
                                    <option value="{{$lop->id}}">{{$lop->tenlop}}</option>
                                @endforeach
                            </select>

                            <div class="more_g">
                                <input type="submit" name="timkiem" value="Tìm kiếm" style="border-radius:0 !important; height:37px !important; margin-top:-5px!important;"/>
                            </div>
                        </form>
                    </div>

                    <div class="class_list clearfix">
                        @if($dslop->count() > 0)
                            @foreach($dslop as $row)
                            <div class="item_c">
                                <div class="c_ttl">LỚP CHƯA GIAO
                                    <p class="c_ms">Mã lớp<br>{{$row->malop}}</p>
                                </div>
                                <div class="c_content" style="height: 350px; line-height: 1.75; font-size:13pt;">
                                    <p>Môn dạy: <span style="color: #008DCA; font-weight: bold;">{{$row->monday}}</span></p>
                                    <p>Lớp dạy: <span style="color: #008DCA; font-weight: bold;">{{$row->lopday}}</span></p>
                                    <?php
                                        $loailop = '';
                                        switch($row->loailop)
                                        {
                                            case 1: $loailop = 'Lớp thường'; break;
                                            case 2: $loailop = 'Lớp chất lượng cao'; break;
                                            default: $loailop = 'Lớp đảm bảo';
                                        }
                                    ?>
                                    <p>Loại lớp: <span style="color: #008DCA; font-weight: bold;">{{$loailop}}</span></p>
                                    <p>Địa chỉ: <a onclick="window.open('https://www.google.com/maps/place/{{urlencode("$row->diachi")}}');" style="border: #008DCA solid 1px; padding: 1px; color: #008DCA; cursor: pointer;" title="Bấm vào để xem bản đồ">{{$row->diachi}}</a></p>
                                    <p>Dạy: <span style="color: #008DCA;">{{$row->sobuoihoc}} buổi/tuần</span></p>
                                    <p>Giờ học: <span style="color: #008DCA;">{{$row->thoigianhoc}}</span></p>
                                    <p>Lương: <span style="color: #008DCA;">{{number_format($row->luong,0,"",".")}} <sup>đ</sup>/tháng</span></p>
                                    <p>Thông tin: <span style="color: #008DCA;">{{$row->thongtin}}</span></p>
                                    <p>Yêu cầu: <span style="color: #008DCA;">{{$row->yeucau}}</span></p>
                                </div>
                                <div class="more_g clearfix">
                                   <a href="{{route('chitietlop', $row->slug)}}">ĐĂNG KÝ DẠY</a>
                                </div>
                            </div>
                        @endforeach
                        @endif
                    </div>
                    <div class="lgs_page clearfix" style="float: right;">{{$dslop->links()}}</div>
                </div>
            </div>
@endsection

@section('footer')
    <script src="{{asset('js/danhsachlop.js?v=4.1')}}"></script>
@endsection
