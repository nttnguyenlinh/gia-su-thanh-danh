@extends('layouts.home')
@section('title', '')
@section('canonical', Route('home'))
@section('url', Route('home'))
@section('content')
    <div class="pageBody clearfix">
        <div class="pageBodyInner clearfix">
            <div class="col_c">
                <details open>
                    <summary>
                        <h1 class="summary-title uppercase">Giới thiệu</h1>
                        <div class="summary-chevron-up">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                        </div>
                    </summary>
                    <div class="summary-content">
                        {!! $gioithieu->noidung !!}
                    </div>
                    <div class="summary-chevron-down">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg>
                    </div>
                </details>
                <details>
                    <summary>
                        <h1 class="summary-title uppercase">Quý phụ huynh cần biết</h1>
                        <div class="summary-chevron-up">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                        </div>
                    </summary>
                    <div class="summary-content">
                        {!! $phuhuynhcb->noidung !!}
                    </div>
                    <div class="summary-chevron-down">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg>
                    </div>
                </details>
                <details>
                    <summary>
                        <h1 class="summary-title uppercase">Gia sư cần biết</h1>
                        <div class="summary-chevron-up">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                        </div>
                    </summary>
                    <div class="summary-content">
                        {!! $giasucb->noidung !!}

                    </div>
                    <div class="summary-chevron-down">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg>
                    </div>
                </details>
                <details>
                    <summary>
                        <h1 class="summary-title uppercase">Các loại lớp học</h1>
                        <div class="summary-chevron-up">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                        </div>
                    </summary>
                    <div class="summary-content">
                        {!! $cacloailop->noidung !!}
                    </div>
                    <div class="summary-chevron-down">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg>
                    </div>
                </details>
                <br>
                <div class="gioithieu clearfix">
                    <div class="gt_l">
                        <img src="/thumb.php?src={{asset('images/gia-su-thanh-danh-day-kem-tai-nha-8.jpg?v=4.1')}}&w=315&h=212" style="border-radius: 10px; margin-top:30px;"/>
                    </div>
                    <div class="gt_r">
                        <p style="text-align:center;"><img src="/thumb.php?src={{asset('images/gia-su-thanh-danh-day-kem-tai-nha-footer.png?v=4.1')}}"/></p>
                        <p style="text-indent: 30px;">Hãy đến với <a href="{{route('home')}}" class="uppercase">{{config('app.name')}}</a> để có được sự lựa chọn đúng đắn.
                            Với sứ mệnh mang lại niềm hạnh phúc cho quý phụ huynh trước sự tiến bộ của con em trong học tập.
                        </p>
                    </div>
                </div>
            </div>
@endsection

@section('footer')
    <script type="text/javascript" src="{{asset('js/faq.js?v=4.1')}}"></script>
@endsection
