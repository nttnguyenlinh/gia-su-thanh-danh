@extends('layouts.home')
@section('title', 'Tài liệu học tập - ')
@section('canonical', Route('tailieu'))
@section('url', Route('tailieu'))
@section('content')
    <div class="pageBody clearfix">
        <div class="pageBodyInner clearfix">
            <div class="col_c">
                <div class="dsgsdk">
                    <h1>TÀI LIỆU HỌC TẬP</h1>
                </div>
                <div class="f_inner">
                    @if($tailieu->count() > 0)
                        @foreach($tailieu as $row)
                            <div class="article-wrapper">
                                <article>
                                    <a href="{{route('chitiettailieu', $row->slug)}}">
                                        <div class="img-wrapper">
                                            @if(!empty($row->anhbia))
                                                <img src="/thumb.php?src={{asset('storage/'.$row->anhbia . '?v=4.1')}}&w=150&h=150">
                                            @else
                                                <img src="/thumb.php?src={{asset('storage/gia-su-thanh-danh-day-kem-tai-nha.jpg?v=4.1')}}&w=150&h=150">
                                            @endif
                                        </div>
                                        <h1>{{$row->tieude}}</h1>
                                        <p>{{strlen($row->mota)<= 191 ? $row->mota : substr($row->mota,0,191)}}...</p>
                                    </a>
                                </article>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
@endsection
@section('footer')
    <link rel="stylesheet" href="{{asset('css/tailieuhoctap.css?v=4.1')}}">
@endsection
