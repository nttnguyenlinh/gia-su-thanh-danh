@extends('layouts.home')
@section('title', 'Tin tức - ')
@section('canonical', Route('tintuc'))
@section('url', Route('tintuc'))
@section('content')
    <div class="pageBody clearfix">
        <div class="pageBodyInner clearfix">
            <div class="col_c">
                <div class="dsgsdk">
                    <h1>TIN TỨC</h1>
                </div>
                <div class="f_inner">
                    @if($tintuc->count() > 0)
                        @foreach($tintuc as $row)
                            <div class="article-wrapper">
                                <article>
                                    <a href="{{route('chitiettintuc', $row->slug)}}">
                                        <div class="img-wrapper">
                                                @if(!empty($row->anhbia))
                                                    <img src="/thumb.php?src={{asset('storage/'.$row->anhbia . '?v=4.1')}}&w=150&h=150">
                                                @else
                                                    <img src="/thumb.php?src={{asset('storage/gia-su-thanh-danh-day-kem-tai-nha.jpg?v=4.1')}}&w=150&h=150">
                                                @endif
                                        </div>
                                        <h1>{{$row->tieude}}</h1>
                                        <p>{{strlen($row->mota)<= 191 ? $row->mota : substr($row->mota,0,191)}}...</p>
                                    </a>
                                </article>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
@endsection

@section('footer')
    <link rel="stylesheet" href="{{asset('css/tintuc.css?v=4.1')}}">
@endsection
