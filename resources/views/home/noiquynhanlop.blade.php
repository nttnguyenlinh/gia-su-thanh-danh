@extends('layouts.home')
@section('title', 'Nội quy nhận lớp - ')
@section('canonical', Route('noiquy'))
@section('url', Route('noiquy'))
@section('content')
    <div class="pageBody clearfix">
        <div class="pageBodyInner clearfix">
            <div class="col_c">
                <div class="dsgsdk">
                    <h1>NỘI QUY NHẬN LỚP</h1>
                </div>
                @if(!empty($noiquy))
                    {!! $noiquy->noidung !!}
                @endif
            </div>
@endsection
