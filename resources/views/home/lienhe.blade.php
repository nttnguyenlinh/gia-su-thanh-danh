@extends('layouts.home')
@section('title', 'Thông tin liên hệ - ')
@section('canonical', Route('lienhe'))
@section('url', Route('lienhe'))
@section('content')
    <div class="pageBody clearfix">
        <div class="pageBodyInner clearfix">
            <div class="col_c">
                <div class="dsgsdk">
                    <h1>THÔNG TIN LIÊN HỆ</h1>
                </div>
                @if(!empty($gioithieu))
                    {!! $gioithieu->noidung !!}
                @endif
            </div>
@endsection

@section('footer')

@endsection
