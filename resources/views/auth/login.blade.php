<!DOCTYPE html>
<html lang="vi">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="content-language" content="vi">
    <meta http-equiv="Cache-control" content="private, max-age=604800">
    <meta http-equiv="REFRESH" content="1800">
    <title>Đăng nhập - {{config('app.title')}}</title>

    <!-- ROBOTS -->
    <meta name="googlebot" content="noarchive" />
    <meta name="robots" content="noarchive" />

    <!-- GENERAL GOOGLE SEARCH META -->
    <meta name="abstract" content="{{config('app.title')}}">
    <meta rel="canonical" href="@yield('canonical')">
    <meta name="RATING" content="GENERAL">
    <meta name="description" content="{{config('app.description')}}">
    <meta name="keywords" content="{{config('app.keywords')}}">
    <meta name="geo.placename" content="Ho Chi Minh, Viet Nam">
    <meta name="geo.posion" content="Ho Chi Minh, Viet Nam">
    <meta property="og:title" content="@yield('title'){{config('app.title')}}">
    <meta property="og:url" content="@yield('url')">
    <meta property="og:site_name" content="{{config('app.name')}}">
    <meta property="og:description" content="{{config('app.description')}}">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="vi_VN">
    <meta property="og:image" content="{{asset(config('app.banner').'?v=4.1')}}">
    <meta property="og:image:secure_url" content="{{asset(config('app.banner').'?v=4.1')}}">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:rich_attachment" content="true">
    <meta name="google-site-verification" content="{{config('google.verification')}}">
    <meta name="msvalidate.01" content="{{config('bing.verification')}}">
    <!-- FACEBOOK OPEN GRAPH -->
    <meta property="fb:app_id" content="">
    <meta property="article:publisher" content="{{config('facebook.url')}}">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="{{config('app.description')}}">
    <meta name="twitter:title" content="@yield('title'){{config('app.title')}}">
    <meta name="twitter:image" content="{{asset(config('app.banner').'?v=4.1')}}">
    <meta name="author" content="giasuthanhdanh.com, Nguyễn Linh - ntt.nguyenlinh@gmail.com">
    <meta name="copyright" content="Copyright 2019 - {{config('app.name')}}">
    <link rel='shortlink' href='{{config('app.url')}}'>

    <link rel="icon" href="{{asset(config('app.icon').'?v=4.1')}}" type="image/x-icon">
    <link rel="apple-touch-icon image_src" href="{{asset(config('app.icon').'?v=4.1')}}">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css?v=4.1">
    <link rel="stylesheet" href="{{asset('css/login.css?v=4.1')}}">
    <link rel="stylesheet" href="{{asset('css/sweetalert2.min.css?v=4.1')}}">
</head>

<body>
<div id="login">
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="{{route('login')}}" method="post">
                        @csrf
                        <h1 class="text-center text-info">GIA SƯ ĐĂNG NHẬP</h1>
                        <div class="form-group">
                            <label for="cmnd" class="text-info">CMND/CCCD:</label><br>
                            <input id="cmnd" type="text"
                                   class="form-control @error('cmnd') is-invalid @enderror"
                                   name="cmnd" value="{{ old('cmnd') }}" required
                                   autocomplete="cmnd" placeholder="CMND/CCCD" autofocus>
                            @error('cmnd')
                            <span class="error">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password" class="text-info">Mật khẩu:</label><br>
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror"
                                   name="password" required autocomplete="current-password"
                                   placeholder="Mật khẩu">
                            @error('password')
                            <span class="error"> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="remember" class="text-info">
                                <span>
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                </span>
                                <span>Ghi nhớ đăng nhập</span>
                            </label><br>
                            <input type="submit" class="btn btn-info btn-md" value="Đăng nhập">
                        </div>
                        <div id="forgot-link" class="text-right">
                            @if (Route::has('password.request'))
                                @if(config('mail.turnon') == 'on')
                                    <a class="text-info" href="{{route('password.request') }}">Quên mật khẩu?</a>
                                @else
                                    <a href="javascript:void(0)" class="text-info" id="hiddenpw" style="font-style:italic; font-size:10pt; float:right;">Quên mật khẩu?</a>
                                @endif
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/jquery-3.4.1.min.js?v=4.1')}}"></script>
<script src="{{asset('js/jquery.validate.min.js?v=4.1')}}"></script>
<script src="{{asset('js/sweetalert2.min.js?v=4.1')}}"></script>
<script src="{{asset('js/login.js?v=4.1')}}"></script>
</body>
</html>
