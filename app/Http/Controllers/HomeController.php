<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GiaSu;
use App\BaiViet;
class HomeController extends Controller
{

    public function index()
    {
        $gioithieu = BaiViet::select('noidung')->where('danhmuc', 5)->get()->first();
        $phuhuynhcb = BaiViet::select('noidung')->where('danhmuc', 6)->get()->first();
        $giasucb = BaiViet::select('noidung')->where('danhmuc', 7)->get()->first();
        $cacloailop = BaiViet::select('noidung')->where('danhmuc', 8)->get()->first();
        return view('home.index', compact('gioithieu', 'phuhuynhcb', 'giasucb', 'cacloailop'));
    }

    public function kiemtra_cmnd(Request $request)
    {
        $return[] = '';
        $giasu = GiaSu::where('cmnd', $request->cmnd);

        if (!$giasu->exists())
            $return = ['status' => 'error', 'message' => 'Xin lỗi không tìm thấy hồ sơ.'];
        else
            $return = ['status' => 'success', 'message' => 'Xin chào'];
        return json_encode($return);
    }

    public function thumb(Request $request)
    {
        if(!empty($request->src))
        {
            $src = $request->src;
            if(!list($width, $height) = getimagesize($src)) return "Định dạng không hỗ trợ!";
            list($width, $height) = getimagesize($src);

            if(!empty($request->w) && !empty($request->h))
            {
                $w = $request->w;
                $h = $request->h;
            }
            else {
                $w = $width;
                $h = $height;
            }

            header('Content-type: ' . getimagesize($src)['mime']);

            $type = strtolower(substr(strrchr(getimagesize($src)['mime'] ,"/"),1));
            if($type == 'jpeg') $type = 'jpg';
            switch($type){
                case 'bmp': $image = imagecreatefromwbmp($src); break;
                case 'gif': $image = imagecreatefromgif($src); break;
                case 'jpg': $image = imagecreatefromjpeg($src); break;
                case 'png': $image = imagecreatefrompng($src); break;
                case 'webp': $image = imagecreatefromwebp($src); break;
                default : return "Định dạng không hỗ trợ!";
            }

            // resize
            $image_n = imagecreatetruecolor($w, $h);

            // preserve transparency
            if($type == "gif" or $type == "png"){
                imagecolortransparent($image_n, imagecolorallocatealpha($image_n, 0, 0, 0, 127));
                imagealphablending($image_n, false);
                imagesavealpha($image_n, true);
            }
            // Resample
            imagecopyresampled($image_n, $image, 0, 0, 0, 0, $w, $h, $width, $height);
            // Output
            if(empty($request->p))
                switch($type){
                    case 'bmp': imagewbmp($image_n, null, 80); break;
                    case 'gif': imagegif($image_n, null, 80); break;
                    case 'jpg': imagejpeg($image_n, null, 80); break;
                    case 'png': imagepng($image_n, null, 6); break;
                    case 'webp': imagewebp($image_n, null, 80); break;
                }
            else
                switch($type){
                    case 'bmp': imagewbmp($image_n, null, $request->p); break;
                    case 'gif': imagegif($image_n, null, $request->p); break;
                    case 'jpg': imagejpeg($image_n, null, $request->p); break;
                    case 'png': imagepng($image_n, null, 9); break;
                    case 'webp': imagewebp($image_n, null, $request->p); break;
                }
            return response()->json(true);
        }
    }
}
