<?php

namespace App\Http\Controllers;

use App\Mail\adminDangKyDayEmail;
use App\Mail\adminDangKyGiaSuEmail;
use App\Mail\DangkyDayEmail;
use App\Mail\DangkyGiaSuEmail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Http\Requests\DangkyDayRequest;
use Mail, Hash;
use Illuminate\Http\Request, Carbon\Carbon;
use App\GiaSu, App\KhuVuc, App\MonHoc, App\LopHoc, App\PhieuMoLop, App\PhieuNhanLop, App\PhieuDangKy;

class DanhSachLopController extends Controller
{
    public function index()
    {
        $khuvuc = KhuVuc::all();
        $lophoc = LopHoc::all();
        $dslop = PhieuMoLop::where('trangthai', 0)->orderBy('created_at', 'desc')->paginate(6);
        return view('home.danhsachlop', compact('khuvuc','lophoc', 'dslop'));
    }

    public function chitietlop(Request $request)
    {
        $lophoc = LopHoc::all();
        $monhoc = MonHoc::all();
        $khuvuc = KhuVuc::all();

        $chitiet = PhieuMoLop::where('slug', 'like', $request->slug)->first();
        if(!empty($chitiet))
            return view('home.chitietlop', compact('chitiet', 'lophoc', 'monhoc', 'khuvuc'));
        else
            return abort(404);
    }

    public function dangkyday(Request $request)
    {
        if(!empty($request->nhanlop))
        {
            $count = PhieuNhanLop::where('lop', $request->malop)->count();
            if($count >= 10)
            {
                $notification = ['status' => 'error', 'message' => 'Lớp này đã đạt đến số lượng đăng ký cho phép. Mời chọn lớp khác.'];
                return redirect()->back()->with($notification);
            }

            else
            {
                $giasu = GiaSu::where('cmnd', $request->ccmnd)->first();
                if($giasu)
                {
                    if($giasu->trangthai == 1)
                    {
                        $count = PhieuNhanLop::where('lop', $request->malop, '&and')->where('giasu', $giasu->id)->count();
                        if($count == 0)
                        {
                            $phieunhan = new PhieuNhanLop();
                            $phieunhan->lop = $request->malop;
                            $phieunhan->giasu = $giasu->id;
                            $phieunhan->thoigianday = Carbon::createFromFormat('d-m-Y', $request->thoigianday)->format('Y-m-d');
                            $phieunhan->ghichu = $request->ghichu;

                            if($phieunhan->save())
                            {
                                if(config('mail.turnon') == 'on')
                                {
                                    $data = ['hoten' => $giasu->holot . ' ' .$giasu->ten];
                                    Mail::to($giasu->email)->send(new DangkyDayEmail($data));
                                    Mail::to(config('mail.admin_address'))->send(new adminDangKyDayEmail());
                                }

                                $notification = ['status' => 'success', 'message' => 'Thông tin nhận dạy của bạn đã được Trung tâm ghi nhận. Trung tâm sẽ liên hệ lại với bạn để trao đổi và xét duyệt thông tin.'];
                                return redirect()->back()->with($notification);
                            }
                            else
                            {
                                $notification = ['status' => 'error', 'message' => 'Xin lỗi! Trung tâm không thể hoàn tất việc nhận dạy của bạn.'];
                                return redirect()->back()->with($notification);
                            }

                        }
                        else
                        {
                            $notification = ['status' => 'error', 'message' => 'Xin lỗi! Bạn đã đăng ký dạy lớp này rồi, mời bạn chọn lớp khác.'];
                            return redirect()->back()->with($notification);
                        }
                    }
                    else
                    {
                        $notification = ['status' => 'error', 'message' => 'Xin lỗi! Tài khoản của bạn đang bị khoá nên không thể nhận lớp.'];
                        return redirect()->back()->with($notification);
                    }
                }
                else
                {
                    $notification = ['status' => 'error', 'message' => 'Không tìm thấy hồ sơ. Bạn vui lòng đăng ký làm gia sư để đồng hành cùng Trung tâm nhé!'];
                    return redirect()->back()->with($notification);
                }
            }
        }

        if(!empty($request->dangky_day))
        {
            $this->validate($request,[
                'frmholot' => 'required|max:30',
                'frmten' => 'required|max:30',
                'frmngaysinh' => 'required|date',
                'frmtinhthanh' => 'required|max:30',
                'frmdiachi' => 'required|max:30',
                'frmquanhuyen' => 'required|max:30',
                'frmemail' => [
                    'required',
                    'email',
                    'regex:/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/',
                    'unique:gia_su,email'
                ],
                'frmsdt' => [
                    'required',
                    'regex:/^(03[2|3|4|5|6|7|8|9]|05[6|8|9]|07[0|6|7|8|9]|08[1|2|3|4|5|6|8|9]|09[0|1|2|3|4|6|7|8|9])+([0-9]{7})$/',
                    'unique:gia_su,sdt'
                ],
                'frmcmnd' => [
                    'required',
                    'regex:/^((?!(0))[0-9]{9,12})$/',
                    'unique:gia_su,cmnd'
                ],
                'frmtrinhdo' => 'required'
            ],
                [
                    'frmholot.required' => 'Vui lòng nhập họ lót.',
                    'frmholot.max' => 'Họ lót bạn quá dài.',

                    'frmten.required' => 'Vui lòng nhập tên.',
                    'frmten.max' => 'Tên bạn quá dài.',

                    'frmngaysinh.required' => 'Vui lòng chọn ngày sinh.',
                    'frmngaysinh.date' => 'Ngày không hợp lệ.',

                    'frmdiachi.required' => 'Vui lòng nhập địa chỉ.',
                    'frmdiachi.max' => 'Địa chỉ quá dài.',

                    'frmquanhuyen.required' => 'Vui lòng chọn quận huyện.',
                    'frmquanhuyen.max' => 'Quận huyện quá dài.',

                    'frmtinhthanh.required' => 'Vui lòng chọn tỉnh thành.',
                    'frmtinhthanh.max' => 'Tỉnh thành quá dài.',

                    'frmemail.required' => 'Vui lòng nhập email.',
                    'frmemail.email' => 'Email không hợp lệ.',
                    'frmemail.regex' => 'Email không hợp lệ.',
                    'frmemail.unique' => 'Email đã tồn tại.',

                    'frmsdt.required' => 'Vui lòng nhập số ĐT.',
                    'frmsdt.regex' => 'Số ĐT không hợp lệ.',
                    'frmsdt.unique' => 'Số ĐT đã tồn tại.',

                    'frmcmnd.required' => 'Vui lòng nhập số CMND/CCCD.',
                    'frmcmnd.regex' => 'Số CMND/CCCD không hợp lệ.',
                    'frmcmnd.unique' => 'Số CMND/CCCD đã tồn tại.',

                    'frmtrinhdo.required' => 'Vui lòng chọn trình độ.'
                ]
            );

            $phieudk = new GiaSu();
            $phieudk->holot = $request->frmholot;
            $phieudk->ten = $request->frmten;
            $phieudk->ngaysinh = Carbon::createFromFormat('d-m-Y', $request->frmngaysinh)->format('Y-m-d');
            $phieudk->diachi = $request->frmdiachi;
            $phieudk->quanhuyen = $request->frmquanhuyen;
            $phieudk->tinhthanh = $request->frmtinhthanh;
            $phieudk->email = $request->frmemail;
            $phieudk->sdt = $request->frmsdt;
            $phieudk->cmnd = $request->frmcmnd;
            $phieudk->password = Hash::make(str_replace('-', '', $request->frmngaysinh));
            $phieudk->trinhdo = $request->frmtrinhdo;
            $phieudk->thongtinkhac = $request->frmthongtinkhac;

            if($phieudk->save())
            {
                if(config('mail.turnon') == 'on')
                {
                    $data = ['hoten' => $request->frmholot . ' ' . $request->frmten];
                    Mail::to($request->frmemail)->send(new DangkyGiaSuEmail($data));
                    Mail::to(config('mail.admin_address'))->send(new adminDangkyGiaSuEmail());
                }

                $count = PhieuNhanLop::where('lop', $request->lop)->count();
                if ($count >= 10) {
                    $notification = ['status' => 'error', 'message' => 'Lớp này đã đạt đến số lượng đăng ký cho phép. Mời chọn lớp khác.'];
                    return redirect()->back()->with($notification);
                }
                else
                {
                    $giasu = GiaSu::where('cmnd', $request->frmcmnd)->first();
                    $count = PhieuNhanLop::where('lop', $request->lop, '&and')->where('giasu', $giasu->id)->count();
                    if ($count == 0)
                    {
                        $phieunhan = new PhieuNhanLop();
                        $phieunhan->lop = $request->lop;
                        $phieunhan->giasu = $giasu->id;

                        if ($phieunhan->save())
                        {
                            if (!empty($giasu->email) && config('mail.turnon') == 'on')
                            {
                                $data = ['hoten' => $giasu->holot . ' ' .$giasu->ten];
                                Mail::to($giasu->email)->send(new DangkyDayEmail($data));
                            }

                            if(config('mail.turnon') == 'on')
                                Mail::to(config('mail.admin_address'))->send(new adminDangKyDayEmail());

                            $notification = ['status' => 'success', 'message' => 'Thông tin nhận dạy của bạn đã được Trung tâm ghi nhận. Trung tâm sẽ liên hệ lại với bạn để trao đổi và xét duyệt thông tin.'];
                            return redirect()->back()->with($notification);
                        }
                    }
                    else
                    {
                        $notification = ['status' => 'error', 'message' => 'Xin lỗi! Bạn đã đăng ký dạy lớp này rồi, mời bạn chọn lớp khác.'];
                        return redirect('/')->with($notification);
                    }

                }
            }
            else{
                $notification = ['status' => 'error', 'message' => 'Xin lỗi không thể hoàn tất! Bạn vui lòng kiểm tra lại các thông tin.'];
                return redirect()->back()->with($notification)->withInput();
            }
        }

    }

    public function timkiemlop(Request $request)
    {
        $khuvuc = KhuVuc::all();
        $lophoc = LopHoc::all();

        if(!empty($request->timkiem))
        {
            if(!empty($request->quanhuyen))
            {
                $kv = KhuVuc::where('id', $request->quanhuyen)->first();
                $quanhuyen = $kv->tenkv;
            }
            else
                $quanhuyen = "";

            if(!empty($request->lopday))
            {
                $lop = LopHoc::where('id', $request->lopday)->first();
                $lopday = $lop->tenlop;
            }
            else
                $lopday = "";

            $tukhoa = $request->tukhoa;

            $dslop = PhieuMoLop::where(function($q) use ($tukhoa){
                $q->where('malop', 'like', '%'.$tukhoa.'%')->orWhere('monday', 'like', '%'.$tukhoa.'%');
            })
                ->where(function($q2) use ($quanhuyen){
                    $q2->orwhere('diachi', 'like', '%'.$quanhuyen.'%');
                })
                ->where(function($q3) use ($lopday){
                    $q3->orwhere('lopday', 'like', '%'.$lopday.'%');
                })
                ->where('trangthai', 0)
                ->orderBy('created_at', 'desc')->paginate(6);
        }
        else
            $dslop = PhieuMoLop::where('trangthai', 0)->orderBy('created_at', 'desc')->paginate(6);
        return view('home.danhsachlop', compact('khuvuc','lophoc', 'dslop'));
    }
}
