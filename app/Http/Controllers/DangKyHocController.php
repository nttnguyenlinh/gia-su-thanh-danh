<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DangkyHocRequest;
use Mail;
use App\Mail\DangkyHocEmail;
use App\Mail\adminDangKyHocEmail;
use App\PhieuDangKy;
use App\LopHoc;
use App\BaiViet;

class DangKyHocController extends Controller
{
    public function index()
    {
        $lophoc = LopHoc::all();
        $content = BaiViet::select('noidung')->where('danhmuc', 10)->get()->first();
        return view('home.dangkyhoc', compact('lophoc', 'content'));
    }

    public function store(DangkyHocRequest $request)
    {
        $phieudk = new PhieuDangKy();
        $phieudk->hoten = $request->frmhoten;
        $phieudk->diachi = $request->frmdiachi;
        $phieudk->email = $request->frmemail;
        $phieudk->sdt = $request->frmsdt;
        $phieudk->lophoc = $request->frmlophoc;
        $phieudk->monhoc = $request->frmmonhoc;
        $phieudk->loailop = $request->frmloailop;
        $phieudk->sohocsinh = $request->frmsohocsinh;
        $phieudk->hocluc = $request->frmhocluc;
        $phieudk->sobuoihoc = $request->frmsobuoihoc;
        $phieudk->thoigianhoc = $request->frmthoigianhoc;
        $phieudk->yeucau = $request->frmyeucau;
        $phieudk->yeucauthem = $request->frmyeucaukhac;

        if($phieudk->save()){
            if(!empty($request->frmemail) && config('mail.turnon') == 'on')
            {
                $data = ['hoten' => $request->frmhoten];
                Mail::to($request->frmemail)->send(new DangkyHocEmail($data));
            }

            if(config('mail.turnon') == 'on')
                Mail::to(config('mail.admin_address'))->send(new adminDangKyHocEmail());

            $notification = ['status' => 'success', 'message' => 'Cảm ơn! Bạn đã lựa chọn ' . config('app.name') . '. Trung tâm sẽ liên hệ lại với bạn để trao đổi thêm.'];
            return redirect('/')->with($notification);
        }
        else{
            $notification = ['status' => 'error', 'message' => 'Xin lỗi! Bạn vui lòng kiểm tra lại các thông tin.'];
            return redirect()->back()->with($notification)->withInput();
        }
    }
}
