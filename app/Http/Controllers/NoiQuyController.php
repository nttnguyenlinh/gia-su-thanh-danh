<?php

namespace App\Http\Controllers;

use App\BaiViet;
use Illuminate\Http\Request;

class NoiQuyController extends Controller
{
    public function index()
    {
        $noiquy = BaiViet::select('noidung')->where('danhmuc', 3)->get()->first();
        return view('home.noiquynhanlop', compact('noiquy'));
    }

}
