<?php

return [
    'driver' => '',

    'host' => '',

    'port' => '',

    'from' => [
        'address' => '',
        'name' => '',
    ],

    'admin_address' => '',

    'encryption' => '',

    'username' => '',

    'password' => '',

    'sendmail' => '/usr/sbin/sendmail -bs',

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

    'log_channel' => '',
];
