<?php

return [
    'auto_save'         => true,
    'driver'            => 'database',
    'database' => [
        'connection'    => 'mysql',
        'table'         => 'settings',
        'key'           => 'key',
        'value'         => 'value',
    ],

    'json' => [
        'path'          => storage_path() . '/settings.json',
    ],

    'override' => [
        "app.name" => "app_name",
        "app.subname" => "app_subname",
//        "app.debug" => "app_debug",
        "app.url" => "app_url",
        "app.title" => "app_title",
        "app.icon" => "app_icon",
        "app.banner" => "app_banner",
        "app.description" => "app_description",
        "app.keywords" => "app_keywords",
        "app.sdt1" => "app_sdt1",
        "app.sdt2" => "app_sdt2",
        "app.email" => "app_email",
        "app.diachi11" => "app_diachi11",
        "app.diachi1" => "app_diachi1",
        "app.diachi22" => "app_diachi22",
        "app.diachi2" => "app_diachi2",
        "mail.turnon" => "mail_turnon",
        "mail.driver" => "mail_driver",
        "mail.host" => "mail_host",
        "mail.port" => "mail_port",
        "mail.encryption" => "mail_encryption",
        "mail.username" => "mail_username",
        "mail.password" => "mail_password",
        "mail.from.address" => "mail_from_address",
        "mail.from.name" => "mail_from_name",
        "mail.admin_address" => "mail_admin_address",
        "facebook.url" => "facebook_url",
        "facebook.id" => "facebook_id",
        "facebook.greeting" => "facebook_greeting",
        "facebook.color" => "facebook_color",
        "google.verification" => "google_verification",
        "bing.verification" => "bing_verification",
    ],

    'required_extra_columns' => [

    ],
];
