$(document).ready(function(){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    $(window).scroll(function(){
        if($(this).scrollTop() > 100){
            $('#scroll').fadeIn();
        }else{
            $('#scroll').fadeOut();
        }
    });
    $('#scroll').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    $('#slider').Swipe({
        auto: 2000,
        continuous: true
    }).data('Swipe');

    if($.cookie('popup_facebook_box') != 'yes'){
        $('#fb-fanbox').delay(5000).fadeIn('fast');
        $('#fb-closebox, #fb-boxclose').click(function(){
            $('#fb-fanbox').stop().fadeOut('fast');
        });
    }
    $.cookie('popup_facebook_box', 'yes', { path: '/', expires: 1 });
});

<!-- Google Tag Manager & Facebook SDK for JavaScript-->
window.fbAsyncInit = function() {FB.init({xfbml: true, version: 'v4.0'});};
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
<!-- End Google Tag Manager & Facebook SDK for JavaScript -->





