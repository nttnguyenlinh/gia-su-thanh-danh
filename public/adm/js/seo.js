$(document).ready(function() {
    // Scroll to the bottom, in case we're in a tall textarea
    $.fn.putCursorAtEnd = function() {
        return this.each(function() {
            // Cache references
            var $el = $(this), el = this;
            // Only focus if input isn't already
            if (!$el.is(":focus")) {$el.focus();}
            // If this function exists... (IE 9+)
            if (el.setSelectionRange) {
                // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
                var len = $el.val().length * 2;
                // Timeout seems to be required for Blink
                setTimeout(function() {el.setSelectionRange(len, len);}, 1);
            } else {
                // As a fallback, replace the contents with itself
                // Doesn't work in Chrome, but Chrome supports setSelectionRange
                $el.val($el.val());
            }
            // Scroll to the bottom, in case we're in a tall textarea
            // (Necessary for Firefox and Chrome)
            this.scrollTop = 999999;
        });
    };
    var mota = $('#mota');
    mota
        .putCursorAtEnd() // should be chainable
        .on("focus", function() { // could be on any event
            mota.putCursorAtEnd()
        });

    $('#mota').bind('mousedown keyup', function() {
        var characterCount = $(this).val().length,
            current = $('#current'),
            maximum = $('#maximum'),
            theCount = $('#the-count');
        current.text(characterCount);
        if (characterCount >= 155) {
            maximum.css('color', 'red');
            current.css('color', 'red');
            theCount.css('font-weight','bold');
        } else {
            current.css('color', '#666');
            maximum.css('color','#666');
            theCount.css('font-weight','normal');
        }
    });

    $('#imgicon').click(function() {
        selectFile('imgicon');
    });

    function selectFile(elementId) {
        CKFinder.modal({
            skin: "neko",
            resourceType: 'System',
            chooseFiles: true,
            chooseFilesOnDblClick: true,
            width: 800,
            height: 600,
            onInit: function(finder) {
                finder.on('files:choose', function(evt) {
                    var file = evt.data.files.first();
                    var output = document.getElementById(elementId);
                    output.value = file.getUrl();
                    updateFile(file.getUrl(), file.get('name'));
                    $(".dropify-clear").attr('style', 'display:block');
                });

                finder.on('file:choose:resizedImage', function(evt) {
                    var output = document.getElementById(elementId);
                    output.value = evt.data.resizedUrl;
                    updateFile(evt.data.resizedUrl, file.get('name'));
                    $(".dropify-clear").attr('style', 'display:block');
                });
            }
        });
    };
    function updateFile(url, name) {
        $('#bieutuong').val(name);
        $('#imgicon').attr('src', url);
    };
});
