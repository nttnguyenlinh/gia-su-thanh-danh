$(document).ready(function() {
    var xmltag = $('#txtsitemap').val();
    $('textarea').val(vkbeautify.xml(xmltag));

    $(document).on("click", "#sitemap-btn", function() {
        $('#sitemap-btn').button('loading');
        $(".progress-striped").css("display", "block");
        $.ajax({
            url: 'sitemap/generator',
            method: 'GET',
            start_time: new Date().getTime(),
            complete: function (data) {
                function progressbar(percent) {
                    $(".progress-bar-success").css("width", percent + '%');
                }

                var elapsedTime = 0;

                function timer() {
                    if (elapsedTime > 100) {
                        toastr['success']('Sitemap đã được tạo thành công!');
                        $('#sitemap-btn').button('reset');
                        clearInterval(mytimer);
                    } else {
                        $('#sitemap-btn').button('loading');
                        progressbar(elapsedTime);
                    }
                    elapsedTime++;
                }

                var mytimer = setInterval(function () {
                    timer()
                }, (new Date().getTime() - this.start_time) / 100);
            }
        });
    });

    $(document).on("click", "#btn-edit", function() {
        $('#btn-edit').css('display', 'none');
        $('#btn-done').css('display', 'inline');
        $('#txtsitemap').attr("readonly", false);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on("click", "#btn-done", function() {
        $('#btn-done').css('display', 'none');
        $('#btn-edit').css('display', 'inline');
        $('#txtsitemap').attr("readonly", true);
        $.ajax({
            url: 'sitemap/put',
            type: 'post',
            data: {content: $('textarea').val()},
            dataType: 'json',
            success: function (data) {
                toastr[data.status](data.message);
            }
        });
    });
});
