$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#chosen-id").chosen({
        no_results_text: "Không tìm thấy dữ liệu...",
        placeholder_text_single: "Chọn gia sư cần đổi...",
        width: "100%",
    });

    $("#chosen-id").on('change', function() {
        $('#holot').val('');
        $('#ten').val('');
        $.ajax({
            url: '../tools/lay-thong-tin-gia-su',
            type: 'get',
            data: {id: $("#chosen-id").val()},
            dataType: 'json',
            success:function(data){
                $('#holot').val(data.holot);
                $('#ten').val(data.ten);
            }
        });
    });

    $("#chosen-idSDT").chosen({
        no_results_text: "Không tìm thấy dữ liệu...",
        placeholder_text_single: "Chọn gia sư cần đổi...",
        width: "100%",
    });

    $("#chosen-idEMAIL").chosen({
        no_results_text: "Không tìm thấy dữ liệu...",
        placeholder_text_single: "Chọn gia sư cần đổi...",
        width: "100%",
    });

    $("#chosen-idCMND").chosen({
        no_results_text: "Không tìm thấy dữ liệu...",
        placeholder_text_single: "Chọn gia sư cần đổi...",
        width: "100%",
    });

    $("#chosen-idMK").chosen({
        no_results_text: "Không tìm thấy dữ liệu...",
        placeholder_text_single: "Chọn gia sư cần cấp lại...",
        width: "100%",
    });

    $.validator.setDefaults({ignore: ":hidden:not(.chosen-select)" });

    $.validator.addMethod("sdt_regex", function (value) {
        return /^(03[2|3|4|5|6|7|8|9]|05[6|8|9]|07[0|6|7|8|9]|08[1|2|3|4|5|6|8|9]|09[0|1|2|3|4|6|7|8|9])+([0-9]{7})$/.test(value);
    });

    $.validator.addMethod("email_regex", function (value) {
        return /^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/.test(value);
    });

    $.validator.addMethod("cmnd_regex", function (value) {
        return /^((?!(0))[0-9]{9,12})$/.test(value);
    });

    $("#frmToolsSDT").validate({
        rules: {
            id: {required: true},
            sdt: {required: true, sdt_regex: true}
        },

        messages: {
            id: {required: 'Vui lòng chọn gia sư.'},
            sdt: {
                required: 'Vui lòng nhập SĐT.',
                sdt_regex: 'SĐT không hợp lệ!'
            }
        }
    });

    $("#frmToolsCMND").validate({
        rules: {
            id: {required: true},
            cmnd: {required: true, cmnd_regex: true}
        },

        messages: {
            id: {required: 'Vui lòng chọn gia sư.'},
            cmnd: {
                required: 'Vui lòng nhập CMND/CCCD.',
                cmnd_regex: 'CMND/CCCD không hợp lệ!'
            }
        }
    });

    $("#frmToolsEmail").validate({
        rules: {
            id: {required: true},
            email: {required: true, email: true, email_regex: true}
        },

        messages: {
            id: {required: 'Vui lòng chọn gia sư.'},
            email: {
                required: 'Vui lòng nhập email.',
                email: 'Email không hợp lệ!',
                email_regex: 'Email không hợp lệ!'
            }
        }
    });

    $("#sdt").on('change', function() {
        $.ajax({
            url: '../tools/kiem-tra-sdt',
            type: 'get',
            data: {sdt:$(this).val(), id:$('#chosen-idSDT').val()},
            dataType: 'json',
            success:function(data){
                $('.kiemtraSDT').children('#lb_kiemtrasdt').remove();
                $('.kiemtraSDT').append("<label id='lb_kiemtrasdt' class='" + data.status + "'>" +data.message + "</label>");
            }
        });
    });

    $("#email").on('change', function() {
        $.ajax({
            url: '../tools/kiem-tra-email',
            type: 'get',
            data: {email:$(this).val(),id:$('#chosen-idEMAIL').val()},
            dataType: 'json',
            success:function(data){
                $('.kiemtraEMAIL').children('#lb_kiemtraemail').remove();
                $('.kiemtraEMAIL').append("<label id='lb_kiemtraemail' class='" + data.status + "'>" +data.message + "</label>");
            }
        });
    });

    $("#cmnd").on('change', function() {
        $.ajax({
            url: '../tools/kiem-tra-cmnd',
            type: 'get',
            data: {cmnd:$(this).val(),id:$('#chosen-idCMND').val()},
            dataType: 'json',
            success:function(data){
                $('.kiemtraCMND').children('#lb_kiemtracmnd').remove();
                $('.kiemtraCMND').append("<label id='lb_kiemtracmnd' class='" + data.status + "'>" +data.message + "</label>");
            }
        });
    });

    $('.remove').click(function() {
        $('#cmndName').val('');
        $("#fileName").attr('data-title', 'Chưa chọn ảnh');
        $("#choose_cmnd").removeClass('selected');
        $("#choose_cmnd").attr('data-title', 'Chọn');
        $(".remove").css("display", "none");
    });
    $('#choose_cmnd').click(function() {
        selectFile('choose_cmnd');
    });
    function selectFile(elementId) {
        CKFinder.modal({
            skin: "neko",
            resourceType: 'CMND',
            chooseFiles: true,
            chooseFilesOnDblClick: true,
            width: 800,
            height: 600,
            onInit: function(finder) {
                finder.on('files:choose', function(evt) {
                    var file = evt.data.files.first();
                    updateFile(file.getUrl(), file.get('name'));
                    $("#choose_cmnd").addClass('selected');
                    $("#choose_cmnd").attr('data-title', 'Đổi');
                    $(".remove").css("display", "block");
                });

                finder.on('file:choose:resizedImage', function(evt) {
                    updateFile(evt.data.resizedUrl, file.get('name'));
                    $("#choose_cmnd").addClass('selected');
                    $("#choose_cmnd").attr('data-title', 'Đổi');
                    $(".remove").css("display", "block");
                });
            }
        });
    }

    function updateFile(url, name) {
        $("#cmndName").val(name);
        $("#fileName").attr('data-title', name);
    }

});
