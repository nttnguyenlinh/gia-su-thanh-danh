lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
});

$(document).ready(function() {
    $.validator.addMethod("email_regex", function (value) {
        return /^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/.test(value);
    });

    $.validator.addMethod("pass_regex", function (value) {
        return /(?=^.{6,20}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&amp;*()_+}{&quot;:;'?/&gt;.&lt;,])(?!.*\s).*$/.test(value);
    });

    $("#form_profile_password").validate({
        rules: {
            current_password:{required: true},
            new_password: {required: true, pass_regex: true},
            confirm_password: {required: true, equalTo: "#new_password"}
        },

        messages: {
            current_password: {
                required: 'Vui lòng nhập mật khẩu hiện tại.'
            },

            new_password: {
                required: 'Vui lòng nhập mật khẩu mới.',
                pass_regex: 'Mật khẩu từ 6-20 ký tự, bao gồm: chữ thường, hoa, số, ký tự đặc biệt như: !@#$%^*()-_+{}|"/<>.~='
            },
            confirm_password: {
                required: 'Vui lòng nhập lại mật khẩu mới.',
                equalTo: 'Mật khẩu mới không trùng khớp.'
            },
        }
    });

    $("#form_profile_update").validate({
        rules: {
            name:{required: true, maxlength: 30},
            email: {required: true, email:true, email_regex: true},
        },
        messages: {
            name: {
                required: 'Vui lòng nhập tên.',
                maxlength: 'Tên quá dài.'
            },

            email: {
                required: 'Vui lòng nhập địa chỉ email.',
                email: 'Email không hợp lệ.',
                email_regex: 'Email không hợp lệ.',
            }
        }
    });

    $("#form_profile_update").ready(function() {
        function onpaste() {
            if($("#email").val() != null)
            {
                $.ajax({
                    url: 'profile/checkEmail',
                    type: 'get',
                    data: {email:$(this).val()},
                    dataType: 'json',
                    success:function(data){
                        $('.kiemtraEMAIL').children('#lb_kiemtraemail').remove();
                        $('.kiemtraEMAIL').append("<label id='lb_kiemtraemail' class='" + data.status + "'>" +data.message + "</label>");
                    }
                });
            }
        }

        $("#email").bind("change keydown keydress keyup mouseout", function() {
            if($("#email").val() != null)
            {
                $.ajax({
                    url: 'profile/checkEmail',
                    type: 'get',
                    data: {email:$(this).val()},
                    dataType: 'json',
                    success:function(data){
                        $('.kiemtraEMAIL').children('#lb_kiemtraemail').remove();
                        $('.kiemtraEMAIL').append("<label id='lb_kiemtraemail' class='" + data.status + "'>" +data.message + "</label>");
                    }
                });
            }
        });

        $('.remove').click(function() {
            $('#avatarName').val('');
            $("#fileName").attr('data-title', 'Chưa chọn ảnh');
            $("#choose_avatar").removeClass('selected');
            $("#choose_avatar").attr('data-title', 'Chọn');
            $(".remove").css("display", "none");
        });
        $('#choose_avatar').click(function() {
            selectFile('choose_avatar');
        });
        function selectFile(elementId) {
            CKFinder.modal({
                skin: "neko",
                resourceType: 'Images',
                chooseFiles: true,
                chooseFilesOnDblClick: true,
                width: 800,
                height: 600,
                onInit: function(finder) {
                    finder.on('files:choose', function(evt) {
                        var file = evt.data.files.first();
                        updateFile(file.getUrl(), file.get('name'));
                        $("#choose_avatar").addClass('selected');
                        $("#choose_avatar").attr('data-title', 'Đổi');
                        $(".remove").css("display", "block");
                    });

                    finder.on('file:choose:resizedImage', function(evt) {
                        updateFile(evt.data.resizedUrl, file.get('name'));
                        $("#choose_avatar").addClass('selected');
                        $("#choose_avatar").attr('data-title', 'Đổi');
                        $(".remove").css("display", "block");
                    });
                }
            });
        };
        function updateFile(url, name) {
            $("#avatarName").val(name);
            $("#fileName").attr('data-title', name);
        };
    });
});
